#Fruitful Day Coding Test Task

##Setup
System requirements:
``` 
- PHP7.2 + nginx/apache or docker
- composer
```

##Intro
You're now going to participate in a short coding test to evaluate your ability to think logically, to work independently with a given situation and/or environment and develop with a smart, goal-oriented approach.

##Task
You need to create a simple CRUD for online orders. The orders should have (but not limited to) the following attributes:
```
- User identifier
- Order items (multiple values are possible)
- Subtotal amount
- Shipping amount
- Total amount
- Notes
- Delivery date
- Payment method
- Status
```

##Hints
- Try to not spend more than 1 hour to complete the task
- Make sure everything you've implemented works as expected
